
#!/bin/bash
# Script to set a per workspace desktop background in Cinnamon.
# Save as ~/bin/workspace_backgrounds_switcher.sh or ~/.local/bin/workspace_backgrounds_switcher.sh and make executable
# Add an entry in startup applications to launch the script on start-up.
#
# This is a fork from https://github.com/smurphos/nemo_actions_and_cinnamon_scripts
# adapted by Giacomo Zanobini
# you can find it at https://gitlab.com/znbn/cinnamon_scripts

# Store your images in /home/jack/Backgrounds/ folder
# Yeah, I know I could have written ~/Backgrounds/ ...let me know if it works.
# The script is verbose, so you can debug it if you run it from shell

# Set your images here - one per active workspace.
# Add extra WORKSPACE_BACKGROUND[X] entries as necessary.
WORKSPACE_BACKGROUND[0]="/home/jack/Backgrounds/0.png"
WORKSPACE_BACKGROUND[1]="/home/jack/Backgrounds/1.png"
WORKSPACE_BACKGROUND[2]="/home/jack/Backgrounds/2.jpg"
WORKSPACE_BACKGROUND[3]="/home/jack/Backgrounds/3.jpg"

# Main script starts here
echo "Script starting: ${0##*/}"
# Check for existing instances and kill them leaving current instance running
for PID in $(pidof -o %PPID -x "${0##*/}"); do
    if [ "$PID" != $$ ]; then
        echo "The script is already running. Killing the previous instance $PID"
        kill -9 "$PID"
    fi 
done

# Check for existing instance still running and exit if so
if pidof -o %PPID -x "${0##*/}"; then
  echo "The script is still running."
  echo "Kill it, before restarting."
  exit 1
fi

# Monitor for workspace changes and set the background on change.
xprop -root -spy _NET_CURRENT_DESKTOP | while read -r;
  do
    echo "changing background for workspace ${REPLY: -1} to file://${WORKSPACE_BACKGROUND[${REPLY: -1}]} "
    gsettings set org.cinnamon.desktop.background picture-uri "file://${WORKSPACE_BACKGROUND[${REPLY: -1}]}"
  done

echo "Script terminated by user"
