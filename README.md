# cinnamon_scripts

This is a fork from https://github.com/smurphos/nemo_actions_and_cinnamon_scripts
adapted by Giacomo Zanobini

You can find it at https://gitlab.com/znbn/cinnamon_scripts

## workspace_background_switch.sh
Script che cambia l'immagine di sfondo per ogni workspace sul desktop.

Salvare lo script in ~/bin/workspace_backgrounds_switcher.sh o
~/.local/bin/workspace_backgrounds_switcher.sh e renderlo eseguibile.

Aggiungere una entry nelle startup applications per eseguire lo script all'avvio.

Salvare le immagini nella cartella /home/jack/Backgrounds/ oppure modificare 
lo script in base all'utente o meglio modificarlo in modo da poter mettere
 ~/Backgrounds/ (non ho provato se funziona, quindi fatemi sapere).
 
Lo script è molto verboso così puoi lanciarlo da shell e farne il debug

Mettere le immagini (una per ogni workspace) aggiungendo altre righe se necessario
WORKSPACE_BACKGROUND[0]="/home/jack/Backgrounds/0.png"
